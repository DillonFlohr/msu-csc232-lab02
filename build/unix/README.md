# Unix Makefiles
This directory should be your target for Unix Makefiles. To generate the files, type

```
$ cmake -G "Unix Makefiles" ../..
```

at the command line prompt.

/**
 * CSC232 - Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file   Crypto.h
 * @author Jim Daehn <jrd2112@missouristate.edu>
 * @brief  TODO: Describe me.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#ifndef LAB02_CRYPTO_H
#define LAB02_CRYPTO_H

#include <cstdlib>
#include <string>

/**
 * @brief Maximum size of data element held by this Crypto.
 */
static const int MAX_SIZE = 80;

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {

    /**
     * @brief Typedef for size_t
     */
    using index = size_t;

    /**
     * @brief Abstract base class of CSC232 Crypto class hierarchy.
     */
    class Crypto {
    public:
        /**
         * @brief  An accessor method for the length of data stored in this
         *         Crypto instance.
         * @return The length of the data stored by this Crypto instance is
         *         returned.
         * @post   The state of this Crypto instance remains unchanged after
         *         executing this operation.
         */
        virtual size_t length() const = 0;

        /**
         * Return a string representation of the character data stored by this
         * Crypto instance.
         *
         * @return A string representation of the character data stored by this
         *         Crypto is returned.
         */
        virtual std::string toString() const = 0;
    };

}
#endif //LAB02_CRYPTO_H

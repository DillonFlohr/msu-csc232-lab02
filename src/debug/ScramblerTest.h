/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file   ScramblerTest.h
 * @author Jim Daehn <jdaehn@missouristate.edu>
 * @brief  Specification of Scrambler Unit Test.
 * @see    http://sourceforge.net/projects/cppunit/files
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#ifndef SCRAMBLERTEST_H
#define SCRAMBLERTEST_H

#include <cppunit/extensions/HelperMacros.h>
#include <cstdlib>
#include "../release/Crypto.h"
#include "../release/Scrambler.h"

using csc232::index;

static const char INIT_DATA[] = {'a', 'b', 'c'};
static const index FIRST_SLOT{0};
static const index NEXT_FREE_SLOT{3};
static const int EXPECTED_LENGTH{NEXT_FREE_SLOT - FIRST_SLOT};
static const char *EXPECTED_SCRAMBLED_TEXT{"cba"};
static const char *EXPECTED_ORIGINAL_TEXT{"abc"};

/**
 * @brief  Specification of Scrambler Unit Test.
 * @remark DO NOT MODIFY THE SPECIFICATION OR IMPLEMENTATION OF THIS CLASS! ANY
 *         MODIFICATION TO THIS CLASS WILL RESULT IN A GRADE OF 0 FOR THIS LAB!
 */
class ScramblerTest : public CPPUNIT_NS::TestFixture {
CPPUNIT_TEST_SUITE(ScramblerTest);

        CPPUNIT_TEST(testLength);
        CPPUNIT_TEST(testScramble);
        CPPUNIT_TEST(testToString);

    CPPUNIT_TEST_SUITE_END();

public:
    /**
     * Default constructor.
     */
    ScramblerTest();

    /**
     * ScramberTest destructor.
     */
    virtual ~ScramblerTest();

    /**
     * Method that executes before each individual test.
     */
    void setUp();

    /**
     * Method that executes after each individual test.
     */
    void tearDown();

private:
    /**
     * Verify that the Scrambler maintains the proper length for the data it
     * stores.
     */
    void testLength();

    /**
     * Verify that the Scrambler reverses the contents of the data it stores.
     */
    void testScramble();

    /**
     * Verify that hte Scrambler retains the origin data with whic it was
     * instantiated.
     */
    void testToString();

    /**
     * Object under test.
     */
    csc232::Scrambler scrambler = csc232::Scrambler{INIT_DATA,
                                                    FIRST_SLOT,
                                                    NEXT_FREE_SLOT};
};

#endif /* SCRAMBLERTEST_H */

/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Main.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          <FILL ME IN ACCORDINGLY>
 * @brief   Entry point to this application.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include <cstdlib>
#include "Scrambler.h"
#include "Descrambler.h"

/**
 * @brief Entry point to this application.
 * @remark You are encouraged to modify this file as you see fit to gain
 * practice in using objects.
 *
 * @param argc the number of command line arguments
 * @param argv an array of the command line arguments
 * @return EXIT_SUCCESS upon successful completion
 */
int main(int argc, char **argv) {
    const char rawData[] = {'a', 'b', 'c'};
    const char scrambledData[] = {'c', 'b', 'a'};
    csc232::Scrambler scrambler(rawData, 0, 3);
    std::cout << "Scrambler is holding a character array of length: "
              << scrambler.length() << std::endl;
    std::cout << "scrambler.toString()     = " << scrambler.toString()
              << std::endl;
    std::cout << "scrambler.scramble()     = " << scrambler.scramble()
              << std::endl;
    std::cout << "scrambler.toString()     = " << scrambler.toString()
              << std::endl;

    // Demo the descrambler by giving it scrambled data, show that it stores
    // this scrambled data, and that after unscrambling it for a client, it
    // still is holding on to the scrambled data

    csc232::Descrambler descrambler(scrambledData, 0, 3);
    std::cout << "descrambler.toString()   = " << descrambler.toString()
              << std::endl;
    std::cout << "descrambler.unscramble() = " << descrambler.unscramble()
              << std::endl;
    std::cout << "descrambler.toString()   = " << descrambler.toString()
              << std::endl;
    return EXIT_SUCCESS;
}
